import pandas as pd
import numpy as np

import ccobra

class PHM(ccobra.CCobraModel):
    def __init__(self, name='PHM+'):
        super(PHM, self).__init__(
            name, ['syllogistic'], ['single-choice'])

        pred_df = pd.read_csv('PHM.csv')
        self.predictions = dict(
            zip(
                pred_df['Syllogism'].tolist(),
                [x.split(';') for x in pred_df['Prediction']]))
      
    def isDifficult(self, item):
        wm1, wm2, wm3, wm4 = item.wm
        middleValue = (wm1*1.3 + wm2*0.7) / 2
        threshold = self.diff[item.task[0][0]]*10
        if threshold > middleValue*1.3:
            return True
        else:
            return False
    
    def predict(self, item, **kwargs):
        enc_task = ccobra.syllogistic.encode_task(item.task)
        enc_resp = self.predictions[enc_task]
        dec_resp = [ccobra.syllogistic.decode_response(x, item.task) for x in enc_resp]
        experimentalpath = '/home/hippo/Downloads/ccobra-master/benchmarks/syllogistic/models/2012-Khemlani/Atmosphere/Atmosphere.csv'
        pred_df = pd.read_csv(experimentalpath)
        self.predch = dict(zip(pred_df['Syllogism'].tolist(),[x.split(';') for x in pred_df['Prediction']]))
        self.choicesch = self.predch[item.task[0][0]]
        self.choices = [a[0][0] for a in item.choices]
        newsol = None
        lastItem = self.lastIt
        self.lastIt = item
        if not self.isDifficult(item) and not (lastItem and self.isDifficult(lastItem)):
            newsol = self.choices[np.random.randint(0, len(self.choices))]
        if newsol:
            return newsol
        if len([a for a in dec_resp if a in self.choicesch]) > 0:
            dec_resp = [a for a in dec_resp if a in self.choicesch]
        return dec_resp[np.random.randint(0, len(dec_resp))]